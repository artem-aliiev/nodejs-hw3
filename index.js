const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();
const {PORT} = require('./config');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

app.use((err, req, res, next) => {
  res.status(400).json({message: err.message});
  next();
});

const start = async () => {
  try {
    await mongoose.connect(`mongodb+srv://artem:Qwerty@cluster0.kfqz0.mongodb.net/node_hw3?retryWrites=true&w=majority`, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  } catch (e) {
    console.log(e.message);
  }

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}`);
  });
};

start();
