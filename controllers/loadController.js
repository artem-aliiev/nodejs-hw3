const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');

module.exports.getLoads = async (req, res) => {
    const userId = req.user._id;
    const {limit='5', offset='0'} = req.query;
    const loads = await Load.find(
        {created_by: userId},
        {__v: 0},
        {
            limit: parseInt(limit) > 100 ? 5 : parseInt(limit),
            skip: parseInt(offset),
        },
    );

    res.status(200).json({loads});
};

module.exports.addLoad = async (req, res) => {
    const userId = req.user._id;
    const load = new Load({
        ...req.body,
        created_by: userId
    });

    await load.save();

    res.status(200).json({message:'Load created successfully'});
};

module.exports.getActiveLoads = async (req, res) => {
    const userId = req.user._id;
    const load = await Load.findOne({
        assigned_to: userId,
        status: 'ASSIGNED'
    });

    res.json({load});
};

module.exports.setLoadState = async (req, res) => {
    const userId = req.user._id;
    const load = await Load.findOne({
        assigned_to: userId,
        status: 'ASSIGNED'},
        {__v: 0});

    if (!load) {
        res.status(400).json({message: 'There are no loads'});
    }

    switch (load.state) {
        case 'En route to Pick Up':
            load.state = 'Arrived to Pick Up';
            break;
        case 'Arrived to Pick Up':
            load.state = 'En route to delivery';
            break;
        case 'En route to delivery':
            load.state = 'Arrived to delivery';
            load.status = 'SHIPPED';
            break;
    }

    await load.save();

    res.json({message: `Load state changed to '${load.state}'`});

};

module.exports.getLoad = async (req, res) => {
    const userId = req.user._id;
    const load = await Load.findOne({
        created_by: userId,
        _id: req.params.id
    });

    res.json({load});
};

module.exports.updateLoad = async (req, res) => {
    const loadId = req.params.id;
    await Load.findByIdAndUpdate(loadId, req.body);

    res.json({message: 'Load details changed successfully'});
};

module.exports.deleteLoad = async (req, res) => {
    const loadId = req.params.id;

    await Note.findByIdAndDelete(loadId);

    res.status(200).json({message: 'Success'});
};

module.exports.postLoad = async (req, res) => {
    const load = await Load.findById(req.params.id);

    if (load.status !== 'NEW') {
        res.status(400).json({message: 'The status of load should be new'});
    }

    await Load.findByIdAndUpdate({
        _id: req.params.id
    }, {status: 'POSTED'});

    const truck = await Truck.where('assigned_to').ne(null).where('status').equals('IS').findOne();

    if (truck) {
        load.assigned_to = truck.assigned_to;
        load.status = 'ASSIGNED';
        load.state = 'En route to Pick Up';
        load.logs.push({message: `Load assigned to driver with id ${load.assigned_to}`, time: Date.now()});
        await load.save();
        await Truck.findByIdAndUpdate(truck._id, {status: 'OL'});
        res.json({message: 'Load posted successfully', driver_found: true});
    } else {
        load.status = 'NEW';
        await load.save();
        res.json({message: 'Driver was not found', driver_found: false});
    }
};


