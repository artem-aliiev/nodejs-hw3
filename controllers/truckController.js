const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');

module.exports.getTrucks = async (req, res) => {
    const userId = req.user._id;
    const {limit='5', offset='0'} = req.query;
    const trucks = await Truck.find(
        {created_by: userId},
        {__v: 0},
        {
            limit: parseInt(limit) > 100 ? 5 : parseInt(limit),
            skip: parseInt(offset),
        },
    );

    res.json({trucks});
};

module.exports.addTruck = async (req, res) => {
    const userId = req.user._id;
    const {type} = req.body;

    if(type === 'SPRINTER' || type === 'SMALL STRAIGHT' || type === 'LARGE STRAIGHT') {

        const truck = new Truck({
            created_by: userId,
            type: type,
        });

        await truck.save();

        res.status(200).json({message: 'Truck created successfully'});
    } else {
        res.status(400).json({message: 'Incorrect truck type'});
    }
};

module.exports.getTruck = async (req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;
    const truck = await Truck.findOne({_id: truckId}, {__v: 0});

    if (!truck) {
        res.status(400).json({message: `There is no truck with ${truckId} id`});
    }

    res.status(200).json({truck: truck});
};

module.exports.updateTruck = async (req, res) => {
    const truckId = req.params.id;
    const {type} = req.body;

    await Truck.findByIdAndUpdate(truckId, {type});

    res.status(200).json({message: 'Truck details changed successfully'});
};

module.exports.deleteTruck = async (req, res) => {
    const truckId = req.params.id;

    await Truck.findByIdAndDelete(truckId);

    res.status(200).json({message: 'Truck deleted successfully'});
};

module.exports.assignTruck = async (req, res) => {
    const truckId = req.params.id;
    const userId = req.user._id;

    await Truck.findByIdAndUpdate(truckId, {assigned_to:userId});

    res.status(200).json({message: 'Truck assigned successfully'});
};
