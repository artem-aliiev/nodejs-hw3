const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config');
const generator = require('generate-password');
const {User} = require('../models/userModel');

const register = async (req, res) => {
  const {email, password, role} = req.body;

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });
  await user.save();

  res.json({message: 'User created successfully!'});
};

const login = async (req, res) => {
  const {email, password} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      'message': `No user with '${email}' email found`,
    });
  }

  if ( !(bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({
      'message': `Wrong password!`,
    });
  }

  const token = jwt.sign({
    username: user.username, _id: user._id,
  }, JWT_SECRET);
  res.json({message: 'Success!', jwt_token: token});
};

const forgotPassword = async (req, res) => {
  const {email} = req.body;

  const user = await User.findOne({email});

  if (!user) {
    return res.status(400).json({
      'message': `No user with '${email}' email found`,
    });
  }

  const password = generator.generate({
    length: 8,
    numbers: true,
    symbols: true,
  });

  await User.updateOne(
      { _id: user._id },
      { password: await bcrypt.hash(password, 10) }
      );

  await sendPassword({
    from: 'transportation.service.help@gmail.com',
    to: email,
    subject: 'Your new password',
    text: 'Your new password',
    html: `Your new password: ${password}`,
  });

  res.json({message: 'New password sent to your email address'});
};

module.exports = {
  register,
  login,
  forgotPassword,
};
