module.exports = {
    PORT: process.env.PORT || 8080,
    JWT_SECRET: process.env.JWT_SECRET || 'metra',
    DB_NAME: process.env.DB_NAME || 'node_hw2',
    DB_USER: process.env.DB_USER || 'artem',
    DB_PASSWORD: process.env.DB_PASSWORD || 'Qwerty',
    TRUCK_TYPES: [
        {
            type: 'SPRINTER',
            payload: 1700,
            dimensions: { width: 300, length: 250, height: 170 }
        },
        {
            type: 'SMALL STRAIGHT',
            payload: 2500,
            dimensions: { width: 500, length: 250, height: 170 }
        },
        {
            type: 'LARGE STRAIGHT',
            payload: 4000,
            dimensions: { width: 700, length: 350, height: 200 }
        }
    ],
};