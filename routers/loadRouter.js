const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {checkDriverMiddleware, checkShipperMiddleware} = require('./middlewares/checkUserMiddleware.js');
const {getLoads,
    addLoad,
    getActiveLoads,
    setLoadState,
    getLoad,
    updateLoad,
    deleteLoad,
    postLoad
} = require('../controllers/loadController');

router.get('/', asyncWrapper(authMiddleware),
    asyncWrapper(getLoads));
router.post('/', asyncWrapper(authMiddleware),
    asyncWrapper(checkShipperMiddleware),
    asyncWrapper(addLoad));
router.get('/active', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(getActiveLoads));
router.patch('/active/state', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(setLoadState));
router.get('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(getLoad));
router.put('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(checkShipperMiddleware),
    asyncWrapper(updateLoad));
router.delete('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(checkShipperMiddleware),
    asyncWrapper(deleteLoad));
router.post('/:id/post', asyncWrapper(authMiddleware),
    asyncWrapper(checkShipperMiddleware),
    asyncWrapper(postLoad));
router.get('/:id/shipping_info', asyncWrapper(authMiddleware),
    asyncWrapper(checkShipperMiddleware),
    asyncWrapper(getLoad));

module.exports = router;