const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {checkDriverMiddleware} = require('./middlewares/checkUserMiddleware.js');
const {getTrucks,
    addTruck,
    getTruck,
    updateTruck,
    deleteTruck,
    assignTruck} = require('../controllers/truckController');

router.get('/', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(getTrucks));

router.post('/', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(addTruck));

router.get('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(getTruck));

router.put('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(updateTruck));

router.delete('/:id', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(deleteTruck));

router.post('/:id/assign', asyncWrapper(authMiddleware),
    asyncWrapper(checkDriverMiddleware),
    asyncWrapper(assignTruck));

module.exports = router;