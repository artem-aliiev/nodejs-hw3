const {User} = require('../../models/userModel');

module.exports.checkDriverMiddleware = async (req, res, next) => {
    const userId = req.user._id;
    const user = await User.findById(userId);

    if (user.role !== 'DRIVER') {
        res.status(400).json({message: 'You do not have permission for this action'});
    }
    next();
}

module.exports.checkShipperMiddleware = async (req, res, next) => {
    const userId = req.user._id;
    const user = await User.findById(userId);

    if (user.role !== 'SHIPPER') {
        res.status(400).json({message: 'You do not have permission for this action'});
    }
    next();
}