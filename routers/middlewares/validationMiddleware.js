const Joi = require('joi');
const userPasswordPattern = new RegExp('^[a-zA-Z0-9]{3,30}$');

module.exports.validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2 })
            .required(),

        password: Joi.string()
            .pattern(userPasswordPattern),

        role: Joi.string()
    });

    await schema.validateAsync(req.body);
    next();
}
