const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../helpers/helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {getUser, deleteUser, changePassword} = require('../controllers/userController');

router.get('/', asyncWrapper(authMiddleware), asyncWrapper(getUser));
router.delete('/', asyncWrapper(authMiddleware), asyncWrapper(deleteUser));
router.patch('/', asyncWrapper(authMiddleware), asyncWrapper(changePassword));

module.exports = router;