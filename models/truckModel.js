const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String,
        default: null
    },
    type: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: 'IS'
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
