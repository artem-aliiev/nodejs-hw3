const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
    created_by: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    assigned_to: {
        type: mongoose.Types.ObjectId,
        default: null,
        ref: 'User',
    },
    status: {
        type: String,
        required: true,
        default: 'NEW',
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    },
    state: {
        type: String,
        default: 'En route to Pick Up',
        enum: [
            'En route to Pick Up',
            'Arrived to Pick Up',
            'En route to delivery',
            'Arrived to delivery',
        ],
    },
    name: {
        type: String,
        required: true,
    },
    payload: {
        type: Number,
        required: true,
    },
    pickup_address: {
        type: String,
    },
    delivery_address: {
        type: String,
    },
    dimensions: {
        width: {
            type: Number,
            required: true,
        },
        length: {
            type: Number,
            required: true,
        },
        height: {
            type: Number,
            required: true,
        },
    },
    logs: [{
        message: {
            type: String,
            required: true,
        },
        time: {
            type: Date,
            default: Date.now(),
        },
    }],
    created_date: {
        type: Date,
        default: Date.now(),
    },
});

module.exports.Load = mongoose.model('Load', loadSchema);
